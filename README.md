# Resume Website Template

[![MIT License](https://img.shields.io/badge/license-MIT-blue.svg)](https://opensource.org/licenses/mit-license.php)

**Resume Website Template** is a single-page Hugo theme

## Features
* Responsive
* Customizable
* Supports [Google Analytics](https://marketingplatform.google.com/about/analytics/)
* Live chat with [Crisp](https://crisp.chat/) integration

#### Bug Reports & Feature Requests

Please use the [issue tracker](https://gitlab.com/chadporter/resume-website-template/issues) to report any bugs or ask feature requests.

## License

Provided under the terms of the [MIT License](https://gitlab.com/chadporter/resume-website-template/blob/master/LICENSE).

Copyright © 2019, [Chad Porter](https://chadporter.net).
